#include "nSysimp.h"
#include "fifoqueues.h"
#include <stdio.h>

typedef struct {
	int leftLock;
	int rightLock;
	FifoQueue pendingTasks;
} *nLRLock;

/* Evite que se defina nLRLock como void * en nSystem.h */
#define NOVOID_NLRLOCK

#define UNLOCK 0
#define LOCK 1

#include "nSystem.h"

nLRLock nMakeLeftRightLock() {
  nLRLock l;

  l = (nLRLock) nMalloc(sizeof(*l));

  l->leftLock = UNLOCK;
  l->rightLock = UNLOCK;
  l->pendingTasks = MakeFifoQueue();

  return l;
}

int nHalfLock(nLRLock l, int timeout) {
  START_CRITICAL();

  //TRUE si ambos estan bloqueados
  int bothLock = (l->leftLock == LOCK && l->rightLock == LOCK);

  //Si no estan disponibles o hay alguien antes que yo
  //Me pongo como pendiente
  if(bothLock || !EmptyFifoQueue(l->pendingTasks)) {
    if(timeout == -1) { //Sin timeout
      current_task->status = WAIT_LRLOCK_SIDE;
      PutObj(l->pendingTasks, current_task);
      ResumeNextReadyTask();
    }
    else { //Con timeout
      //La tarea se despertara sola despues del timeout
      current_task->status = WAIT_LRLOCK_SIDE_TIMEOUT;
      ProgramTask(timeout);
      PutObj(l->pendingTasks, current_task);
      ResumeNextReadyTask();
    }
  }

  //Se desperto, ya no es pendiente
  DeleteObj(l->pendingTasks, current_task);

  //Hay un side disponible
  if(l->leftLock == UNLOCK) {
  	l->leftLock = LOCK;
  	PutTask(ready_queue, current_task);
  	ResumeNextReadyTask();
  	END_CRITICAL();
  	return LEFT;
  }

  if(l->rightLock == UNLOCK) {
  	l->rightLock = LOCK;
  	PutTask(ready_queue, current_task);
  	ResumeNextReadyTask();
  	END_CRITICAL();
  	return RIGHT;
  }

  //No hay disponibilidad, desperte mal
  PutTask(ready_queue, current_task);

  ResumeNextReadyTask();

  END_CRITICAL();

  return NONE;
}

void nHalfUnlock(nLRLock l, int side) {
  START_CRITICAL();

  //Desbloqueamos
  if(side == LEFT) {
  	l->leftLock = UNLOCK;
  } else { //RIGHT
  	l->rightLock = UNLOCK;
  }

  //TRUE si ambos estan desbloqueados
  int bothUnlock = (l->rightLock == UNLOCK && l->leftLock == UNLOCK);

  //Si hay alguien esperando, se despierta (si se puede)
  if(!EmptyFifoQueue(l->pendingTasks)) {
    nTask t = (nTask) GetObj(l->pendingTasks);

    //Si espera ambos y ambos estan desocupados
    if((t->status == WAIT_LRLOCK_FULL) && bothUnlock) {
      t->status = READY;

      //La gracia de este doble push es que a pesar de entregar
      //la ejecucion, yo soy el que sigue
      //Se utiliza durante todo el codigo la misma logica
      PushTask(ready_queue, current_task);
      PushTask(ready_queue, t);
    } 

    //Si espera ambos y ambos estan desocupados
    else if((t->status == WAIT_LRLOCK_FULL_TIMEOUT)  && bothUnlock){
      CancelTask(t);
      t->status = READY;
      PushTask(ready_queue, current_task);
      PushTask(ready_queue, t);
    }
  
    //Si espera uno, y sabemos que al menos uno esta desocupado
    else if(t->status == WAIT_LRLOCK_SIDE) {
      t->status = READY;
      PushTask(ready_queue, current_task);
      PushTask(ready_queue, t);
    }

    //Si espera uno, y sabemos que al menos uno esta desocupado
    else if(t->status == WAIT_LRLOCK_SIDE_TIMEOUT){
      CancelTask(t);
      t->status = READY;
      PushTask(ready_queue, current_task);
      PushTask(ready_queue, t);
    }
    
    //Ya estaba despierto
    else if(t->status == READY) {
      DeleteTaskQueue(ready_queue, t);
      PushTask(ready_queue, current_task);
      PushTask(ready_queue, t);
    }

    else {
      //No pasa nada, vuelve a la queue
      PushObj(l->pendingTasks, t);
      PutTask(ready_queue, current_task);
    }
  }

  //Nadie esperando, yo me voy a la ready_queue
  else {
  	PushTask(ready_queue, current_task);
  }

  ResumeNextReadyTask();

  END_CRITICAL();
}

int nFullLock(nLRLock l, int timeout) {
  START_CRITICAL();

  //TRUE si no estan ambos desbloqueados
  int someLock = (l->leftLock == LOCK || l->rightLock == LOCK);

  //Si hay alguno ocupado o hay alguien antes que yo
  //Me pongo como pendiente
  if(someLock || !EmptyFifoQueue(l->pendingTasks)) {
    if(timeout == -1) { //Sin timeout
      current_task->status = WAIT_LRLOCK_FULL;
      PutObj(l->pendingTasks, current_task);
      ResumeNextReadyTask();
    }
    else { //Con timeout
      //La tarea se despertara sola despues del timeout
      current_task->status= WAIT_LRLOCK_FULL_TIMEOUT;
      ProgramTask(timeout);
      PutObj(l->pendingTasks, current_task);
      ResumeNextReadyTask();
    }
  }

  //Desperte, me salgo de las pendientes
  DeleteObj(l->pendingTasks, current_task);

  //Si puede tomarlos ambos, los toma
  if(l->leftLock == UNLOCK && l->rightLock == UNLOCK) {
  	l->leftLock = LOCK;
  	l->rightLock = LOCK;

  	PutTask(ready_queue, current_task);
  	ResumeNextReadyTask();
  	END_CRITICAL();

  	return TRUE;
  }

  //No pudo tomar ninguno
  PutTask(ready_queue, current_task);
  ResumeNextReadyTask();
  END_CRITICAL();

  return FALSE;
}

void nFullUnlock(nLRLock l) {
  START_CRITICAL();

  //Desbloqueamos
  l->leftLock = UNLOCK;
  l->rightLock = UNLOCK;

  //Si hay alguien esperando
  if(!EmptyFifoQueue(l->pendingTasks)) {
  	nTask t = (nTask) GetObj(l->pendingTasks);

    //Esta esperando ambos, se los damos
  	if(t->status == WAIT_LRLOCK_FULL) {
  	  t->status = READY;
  	  PushTask(ready_queue, current_task);
  	  PushTask(ready_queue, t);
    } 

    //Esta esperando ambos, se los damos
    else if(t->status == WAIT_LRLOCK_FULL_TIMEOUT){
      CancelTask(t);
      t->status = READY;
      PushTask(ready_queue, current_task);
      PushTask(ready_queue, t);
    }

    //Esta esperando uno, le damos uno
    else if(t->status == WAIT_LRLOCK_SIDE) {
      t->status = READY;
      
      PushTask(ready_queue, current_task);
      PushTask(ready_queue, t);
      ResumeNextReadyTask();
      
      //Vemos si el siguiente tambien puede tomar uno
      //Solo revisamos los half lock
      if(!EmptyFifoQueue(l->pendingTasks)) {
        t = (nTask) GetObj(l->pendingTasks);

        if(t->status == WAIT_LRLOCK_SIDE) {
	        t->status = READY;
	        PushTask(ready_queue, current_task);
	        PushTask(ready_queue, t);
	      } 

      	else if(t->status == WAIT_LRLOCK_SIDE_TIMEOUT) {
  	      CancelTask(t);
  	      t->status = READY;
  	      PushTask(ready_queue, current_task);
  	      PushTask(ready_queue, t);
      	}

        else { //No podia tomar, de vuelta a la queue
          PushObj(l->pendingTasks, t);
        }
      }
    }

    //Esta esperando uno, le damos uno
    else if(t->status == WAIT_LRLOCK_SIDE_TIMEOUT) {
      CancelTask(t);

      t->status = READY;
      
      PushTask(ready_queue, current_task);
      PushTask(ready_queue, t);
      ResumeNextReadyTask();
      
      //Lo mismo que el anterior
      if(!EmptyFifoQueue(l->pendingTasks)) {
      	t = (nTask) GetObj(l->pendingTasks);

      	if(t->status == WAIT_LRLOCK_SIDE) {
          t->status = READY;
          PushTask(ready_queue, current_task);
          PushTask(ready_queue, t);
        } 

      	else if(t->status == WAIT_LRLOCK_SIDE_TIMEOUT){
          CancelTask(t);
          t->status = READY;
          PushTask(ready_queue, current_task);
          PushTask(ready_queue, t);
        }

        else { //No podia tomar, de vuelta a la queue
          PushObj(l->pendingTasks, t);
        }
  	  }
    }
    
    //Ya estaba despierto
  	else if(t->status == READY) {
  		DeleteTaskQueue(ready_queue, t);  	  
      PushTask(ready_queue, current_task);
  	  PushTask(ready_queue, t);
  	}

    //No pudo hacer nada, vuelve a ser pendiente
    else {
      PushObj(l->pendingTasks, t);
      PutTask(ready_queue, current_task);
    }
  }

  //Nadie mas esta esperando, asi que lo intento de nuevo
  else {
  	PushTask(ready_queue, current_task);
  }

  ResumeNextReadyTask();

  END_CRITICAL();
}

void nDestroyLeftRightLock(nLRLock l) {
  DestroyFifoQueue(l->pendingTasks);
  nFree(l);
}
